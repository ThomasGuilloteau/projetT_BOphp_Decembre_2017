<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// ajout des données formule dans la table
$req_formule_ajout = $bdd->prepare('INSERT INTO formules(formuleNom, formulePrix, formuleDescription) VALUES (:nom, :prix, :description)');
$req_formule_ajout->execute(array(
  'nom' => $_POST['nom'],
  'prix' => $_POST['prix'],
  'description' => $_POST['description']
));
header('location:../formules.php?ajouter=1');
?>
