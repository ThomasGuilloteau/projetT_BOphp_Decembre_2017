<?php
//activation de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
?>
<!-- Page de paramétrage Equipe -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Formules</title>
  </head>
  <body>
      <!-- Partie Ajout d'item -->
    <div class='ajouter'>
      <title class='title'> Ajouter une formule</title>
    <form method="post" action="/action/formuleAjout.php">
      <div class="form-group">
        <label> Nom de la formule</label>
        <input type="text" class="form-control" name="nom" placeholder="">
      </div>


      <div class="form-group">
        <label>Prix de la formule</label>
        <input type="text" class="form-control" name="prix"  placeholder="">
      </div>

      <div class="form-group">
        <label>Description de la formule</label>
        <input type="text" class="form-control" name="description"  placeholder="">
      </div>
      <button type="submit" class="btn btn-light">Ajouté</button>
</div>
  <!-- Message de confirmation -->
      <?php
            if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
              echo "
              <div class = 'alert alert-danger'>
              Votre formule a été ajouter
              </div>";
             }
      ?>
<!-- Partie modif d'item -->

      <div class='modifier'>
        <title class='title'> Modifier une formule</title>
      <form method="post" action="/action/formuleModif.php">
        <div class="form-group">
          <label> Nom de la formule</label>
          <input type="text" class="form-control" name="nom" placeholder="">
        </div>


        <div class="form-group">
          <label>Prix de la formule</label>
          <input type="text" class="form-control" name="prix"  placeholder="">
        </div>

        <div class="form-group">
          <label>Description de la formule</label>
          <input type="text" class="form-control" name="description"  placeholder="">
        </div>
        <button type="submit" class="btn btn-light">Modifié</button>
      </div>
        <!-- Message de confirmation -->
        <?php
              if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                echo "
                <div class = 'alert alert-danger'>
                Votre formule a été modifié
                </div>";
               }
        ?>
<!-- Partie Supression d'item -->
        <div class='supprimer'>
          <title class='title'> Supprimé une formule</title>
        <form method="post" action="/action/formuleSuppr.php">
          <div class="form-group">
            <label> Nom de la formule</label>
            <input type="text" class="form-control" name="nom" placeholder="">
          </div>


          <div class="form-group">
            <label>Prix de la formule</label>
            <input type="text" class="form-control" name="prix"  placeholder="">
          </div>

          <div class="form-group">
            <label>Description de la formule</label>
            <input type="text" class="form-control" name="description"  placeholder="">
          </div>
          <button type="submit" class="btn btn-light">Supprimé</button>
        </div>
          <!-- Message de confirmation -->
          <?php
                if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                  echo "
                  <div class = 'alert alert-danger'>
                  Votre formule a été supprimé
                  </div>";
                 }
          ?>

  </body>
</html>
