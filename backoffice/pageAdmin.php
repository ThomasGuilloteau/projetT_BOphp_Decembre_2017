<?php
//activation de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
?>

<!-- Page d'accueil Admin -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Admin</title>
  </head>
  <body>

    <!-- Message de confirmation de connexion -->
    <?php
    if(isset($_GET['error'])&& $_GET['error'] == 0){
        echo "
        <div class = 'alert alert-danger'>
        Bienvenue sur la page administrateur
        </div>";
      }
       ?>
<!-- Choix de la catégorie a parametre -->

       <div class='liens'>
        <div class='formule'>
         <ul>
           <li ><a href='formules/formules.php'class='modifFormule'> Modifier mes Formules </a></li>
         </ul>
        </div>

      <div class='evenement'>
       <ul>
          <li ><a href='evenement/evenement.php'class='modifFormule'> Modifier mes Evènements </a></li>
         </ul>
      </div>

      <div class='partenaires'>
       <ul>
         <li ><a href='partenaires/partenaire.php'class='modifPartenaire'> Modifier mes partenaires </a></li>
       </ul>
      </div>

      <div class='equipe'>
       <ul>
         <li ><a href='equipe/equipe.php'class='modifEquipe'> Modifier mes Equipiers </a></li>
       </ul>
      </div>
<!-- Bouton deco session -->
  </div>
    <button class='deco'><a href='pageDeco.php'> Déconnexion </a></button>
  </body>
</html>
