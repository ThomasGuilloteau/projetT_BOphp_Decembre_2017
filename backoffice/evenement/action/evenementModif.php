<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// Modif des données evenement dans la table
$req_equipe_modif = $bdd->prepare('UPDATE evenements SET evenementNom = :nom, evenementCategorie = :categorie evenementAdresse = :adresse, evenementCP = :cp, evenementDate = :date, evenementDescription = :description'.' WHERE evenementNom = :nomActuel');
$req_equipe_modif->execute(array(
  'nom' => $_POST['nomModif'],
  'nomActuel' => $_POST['nomActuel'],
  'categorie'=> $_POST['categorieModif'],
  'adresse'=> $_POST['adresseModif'],
  'cp'=> $_POST['cpModif'],
  'date'=> $_POST['dateModif'],
  'description'=> $_POST['descriptionModif']
));
header('location:../equipe.php?ajouter=2');
?>
