<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// ajout des données equipe dans la table
$req_evenement_ajout = $bdd->prepare('INSERT INTO evenements (evenementNom, evenementCategorie,
  evenementAdresse, evenementCP, evenementDate, evenementDescription)
   VALUES (:nom, :categorie, :adresse, :cp, :date, :description)');
$req_evenement_ajout->execute(array(
  'nom' => $_POST['nom'],
  'categorie' => $_POST['categorie'],
  'adresse' => $_POST['adresse'],
  'cp' => $_POST['cp'],
  'date' => $_POST['date'],
  'description' => $_POST['description']
  ));

header('location:../evenement.php?ajouter=1');
?>
