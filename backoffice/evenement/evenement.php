<?php
//activation de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
?>
<!-- Page de paramétrage evenement -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Evènement</title>
  </head>
  <body>
    <!-- Partie Ajout d'item -->
    <div class= 'ajouter'>
      <title class='title'> Ajouté un Evènement </title>
    <form method="post" action="/action/evenementAjout.php">
      <div class="form-group">
        <label> Nom de l'évènement</label>
        <input type="text" class="form-control" name="nom" placeholder="">
      </div>


      <div class="form-group">
        <label>Catégorie</label>
        <input type="text" class="form-control" name="categorie"  placeholder="">
      </div>

      <div class="form-group">
        <label>Adresse de l'évènement</label>
        <input type="text" class="form-control" name="adresse"  placeholder="">
      </div>

      <div class="form-group">
        <label>Code Postale l'évènement</label>
        <input type="number" class="form-control" name="cp"  placeholder="">
      </div>

      <div class="form-group">
        <label>Date de l'évènement</label>
        <input type="date" class="form-control" name="date"  placeholder="">
      </div>

      <div class="form-group">
        <label>Description de l'évènement</label>
        <input type="text" class="form-control" name="description"  placeholder="">
      </div>

      <button type="submit" class="btn btn-light">Ajouté</button>
</div>
<!-- Message de confirmation -->
      <?php
            if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
              echo "
              <div class = 'alert alert-danger'>
              Votre évènement a été ajouter
              </div>";
             }
      ?>

      <!-- Partie modif d'item -->

      <div class= 'modifier'>
        <title class='title'> Modifié un Evènement </title>
      <form method="post" action="/action/evenementModif.php">
        <div class="form-group">
          <label> Nom de l'évènement actuel</label>
          <input type="text" class="form-control" name="nomActuel" placeholder="">
        </div>

        <div class="form-group">
          <label> Nom du nouvel évènement</label>
          <input type="text" class="form-control" name="nomModif" placeholder="">
        </div>


        <div class="form-group">
          <label>Catégorie</label>
          <input type="text" class="form-control" name="categorieModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Adresse de l'évènement</label>
          <input type="text" class="form-control" name="adresseModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Code Postale l'évènement</label>
          <input type="number" class="form-control" name="cpModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Date de l'évènement</label>
          <input type="date" class="form-control" name="dateModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Description de l'évènement</label>
          <input type="text" class="form-control" name="descriptionModif"  placeholder="">
        </div>

        <button type="submit" class="btn btn-light">Modifié</button>
      </div>
      <!-- Message de confirmation -->
        <?php
              if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 2){
                echo "
                <div class = 'alert alert-danger'>
                Votre évènement a été Modifié
                </div>";
               }
        ?>
        <!-- Partie Supression d'item -->

        <div class= 'supprimer'>
          <title class='title'> Supprimé un Evènement </title>
        <form method="post" action="/action/evenementSuppr.php">
          <div class="form-group">
            <label> Nom de l'évènement</label>
            <input type="text" class="form-control" name="nom" placeholder="">
          </div>


          <div class="form-group">
            <label>Catégorie</label>
            <input type="text" class="form-control" name="categorie"  placeholder="">
          </div>

          <div class="form-group">
            <label>Adresse de l'évènement</label>
            <input type="text" class="form-control" name="adresse"  placeholder="">
          </div>

          <div class="form-group">
            <label>Code Postale l'évènement</label>
            <input type="number" class="form-control" name="cp"  placeholder="">
          </div>

          <div class="form-group">
            <label>Date de l'évènement</label>
            <input type="date" class="form-control" name="date"  placeholder="">
          </div>

          <div class="form-group">
            <label>Description de l'évènement</label>
            <input type="text" class="form-control" name="description"  placeholder="">
          </div>

          <button type="submit" class="btn btn-light">Supprimé</button>
        </div>
        <!-- Message de confirmation -->
          <?php
                if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                  echo "
                  <div class = 'alert alert-danger'>
                  Votre évènement a été supprimé
                  </div>";
                 }
          ?>




  </body>
</html>
