<?php
//activation de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
?>
<!-- Page de paramétrage Equipe -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Equipe</title>
  </head>
  <body>

    <!-- Partie Ajout d'item -->
    <div class='ajouter'>
      <title class='tilte'> Ajouter un equipier </title>
    <form method="post" action="/action/equipeAjout.php">
      <div class="form-group">
        <label> Nom de votre équipier</label>
        <input type="text" class="form-control" name="nom" placeholder="">
      </div>


      <div class="form-group">
        <label>Prénom de votre équipier</label>
        <input type="text" class="form-control" name="prenom"  placeholder="">
      </div>

      <div class="form-group">
        <label>Age de votre équipier</label>
        <input type="number" class="form-control" name="age"  placeholder="">
      </div>

      <div class="form-group">
        <label>Email de votre équipier</label>
        <input type="email" class="form-control" name="email"  placeholder="">
      </div>

      <div class="form-group">
        <label>Poste de votre équipier</label>
        <input type="text" class="form-control" name="poste"  placeholder="">
      </div>

      <button type="submit" class="btn btn-light">Ajouté</button>
    </div>
    <!-- Message de confirmation -->
      <?php
            if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
              echo "
              <div class = 'alert alert-danger'>
              Votre Equipier a été ajouter
              </div>";
             }
      ?>

<!-- Partie modif d'item -->

<div class= 'modifier'>
  <title class='tilte'>Modifier un equipier </title>
      <form method="post" action="/action/equipeModif.php">
        <div class="form-group">
          <label> Nom de votre équipier actuel</label>
          <input type="text" class="form-control" name="nomActuel" placeholder="">
        </div>

        <div class="form-group">
          <label> Nom de votre nouveau équipier</label>
          <input type="text" class="form-control" name="nomModif" placeholder="">
        </div>


        <div class="form-group">
          <label>Prénom de votre équipier</label>
          <input type="text" class="form-control" name="prenomModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Age de votre équipier</label>
          <input type="number" class="form-control" name="ageModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Email de votre équipier</label>
          <input type="email" class="form-control" name="emailModif"  placeholder="">
        </div>

        <div class="form-group">
          <label>Poste de votre équipier</label>
          <input type="text" class="form-control" name="posteModif"  placeholder="">
        </div>

        <button type="submit" class="btn btn-light">Modifié</button>
<!-- Message de confirmation -->
        <?php
              if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 2){
                echo "
                <div class = 'alert alert-danger'>
                Votre Equipier a été Modifier
                </div>";
               }
        ?>

</div>

<!-- Partie Supression d'item -->

<div class= 'modifier'>
  <title class='tilte'>Supprimer un equipier </title>
      <form method="post" action="/action/equipeSuppr.php">

        <div class="form-group">
          <label> Nom de votre équipier</label>
          <input type="text" class="form-control" name="nom" placeholder="">
        </div>


        <div class="form-group">
          <label>Prénom de votre équipier</label>
          <input type="text" class="form-control" name="prenom"  placeholder="">
        </div>

        <div class="form-group">
          <label>Age de votre équipier</label>
          <input type="number" class="form-control" name="age"  placeholder="">
        </div>

        <div class="form-group">
          <label>Email de votre équipier</label>
          <input type="email" class="form-control" name="email"  placeholder="">
        </div>

        <div class="form-group">
          <label>Poste de votre équipier</label>
          <input type="text" class="form-control" name="poste"  placeholder="">
        </div>

        <button type="submit" class="btn btn-light">Supprimé</button>
<!-- Message de confirmation -->
        <?php
              if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                echo "
                <div class = 'alert alert-danger'>
                Votre Equipier a été suprimer.
                </div>";
               }
        ?>

</div>


</body>
</html>
