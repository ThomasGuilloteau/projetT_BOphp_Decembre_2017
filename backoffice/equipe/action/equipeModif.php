<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// Modif des données equipe dans la table
$req_equipe_modif = $bdd->prepare('UPDATE equipiers SET equipierNom = :nom, equipierPrenom = :prenom equipierAge = :age, equipierEmail = :email, equipierPoste = :poste'.' WHERE equipierNom = :nomActuel');
$req_equipe_modif->execute(array(
  'nom' => $_POST['nomModif'],
  'nomActuel' => $_POST['nomActuel'],
  'prenom'=> $_POST['prenomModif'],
  'age'=> $_POST['ageModif'],
  'email'=> $_POST['emailModif'],
  'poste'=> $_POST['posteModif']
));
header('location:../equipe.php?ajouter=2');
?>
