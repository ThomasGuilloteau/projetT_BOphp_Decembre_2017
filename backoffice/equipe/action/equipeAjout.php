<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// ajout des données equipe dans la table
$req_equipe_ajout = $bdd->prepare('INSERT INTO equipiers(equipierNom,
  equipierPrenom, equipierAge, equipierEmail, equipierPoste)
  VALUES (:nom, :prenom, :age, :email, :poste)');
$req_equipe_ajout->execute(array(
  'nom' => $_POST['nom'],
  'prenom' => $_POST['prenom'],
  'age' => $_POST['age'],
  'email' => $_POST['email'],
  'poste' => $_POST['poste']
));
header('location:../equipe.php?ajouter=1');
?>
