<!-- Page de connexion administrateur -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Connexion</title>
  </head>

  <!-- Formulaire de connexion -->

  <body>
    <form method="post" action="/action/connexionAdmin.php">
      <div class="form-group">
        <label> Login</label>
        <input type="text" class="form-control" name="login" placeholder="">
      </div>


      <div class="form-group">
        <label>Mot de passe</label>
        <input type="password" class="form-control" name="mdp"  placeholder="">
      </div>
      <button type="submit" class="btn btn-light">Envoyer</button>
      
<!-- Message d'erreur -->
      <?php
            if(isset($_GET['error'])&& $_GET['error'] == 1){
              echo "
              <div class = 'alert alert-danger'>
              Login ou Mot de passe incorrecte!
              </div>";
             }
      ?>

  </body>
</html>
