<?php
// création de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
//Connexion a la bdd
$bdd = new PDO('mysql:host=localhost;dbname=projetT;charset=utf8', 'root', '');
// ajout des données equipe dans la table
$req_partenaire_ajout = $bdd->prepare('INSERT INTO partenaires(partenaireNom,
  partenaireAdresse, partenaireCP, partenaireVille, partenaireEmail)
  VALUES (:nom, :adresse, :cp, :ville, :email)');
$req_partenaire_ajout->execute(array(
  'nom' => $_POST['nom'],
  'adresse' => $_POST['adresse'],
  'cp' => $_POST['cp'],
  'ville' => $_POST['ville'],
  'email' => $_POST['email']
));
header('location:../partenaire.php?ajouter=1');
?>
