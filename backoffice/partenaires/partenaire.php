<?php
//activation de la session avec condition
session_start();
if(!isset($_SESSION['login'])){
header('location:../pageConnexion.php');
exit();
}
?>
<!-- Page de paramétrage Partenaire -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Partenaires</title>
  </head>
  <body>
        <!-- Partie Ajout d'item -->
    <div class='ajouter'>
      <title class='title'>Ajouter un Partenaire</title>
    <form method="post" action="/action/partenaireAjout.php">
      <div class="form-group">
        <label> Nom du partenaire</label>
        <input type="text" class="form-control" name="nom" placeholder="">
      </div>


      <div class="form-group">
        <label>Adresse du partenaire</label>
        <input type="text" class="form-control" name="adresse"  placeholder="">
      </div>

      <div class="form-group">
        <label>Code Postale</label>
        <input type="number" class="form-control" name="cp"  placeholder="">
      </div>

      <div class="form-group">
        <label>Ville</label>
        <input type="text" class="form-control" name="ville"  placeholder="">
      </div>

      <div class="form-group">
        <label>Adresse email</label>
        <input type="email" class="form-control" name="email"  placeholder="">
      </div>

      <button type="submit" class="btn btn-light">Ajouté</button>
</div>
    <!-- Message de confirmation -->
      <?php
            if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
              echo "
              <div class = 'alert alert-danger'>
              Votre partenaire a été ajouter
              </div>";
             }
      ?>
<!-- Partie modif d'item -->
      <div class='modifier'>
        <title class='title'>Modifié un Partenaire</title>
      <form method="post" action="/action/partenaireModif.php">
        <div class="form-group">
          <label> Nom du partenaire</label>
          <input type="text" class="form-control" name="nom" placeholder="">
        </div>


        <div class="form-group">
          <label>Adresse du partenaire</label>
          <input type="text" class="form-control" name="adresse"  placeholder="">
        </div>

        <div class="form-group">
          <label>Code Postale</label>
          <input type="number" class="form-control" name="cp"  placeholder="">
        </div>

        <div class="form-group">
          <label>Ville</label>
          <input type="text" class="form-control" name="ville"  placeholder="">
        </div>

        <div class="form-group">
          <label>Adresse email</label>
          <input type="email" class="form-control" name="email"  placeholder="">
        </div>

        <button type="submit" class="btn btn-light">Modifié</button>
      </div>
          <!-- Message de confirmation -->
        <?php
              if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                echo "
                <div class = 'alert alert-danger'>
                Votre partenaire a été Modifié
                </div>";
               }
        ?>
<!-- Partie Supression d'item -->
        <div class='supprimer'>
          <title class='title'>Supprimer un Partenaire</title>
        <form method="post" action="/action/partenaireSuppr.php">
          <div class="form-group">
            <label> Nom du partenaire</label>
            <input type="text" class="form-control" name="nom" placeholder="">
          </div>


          <div class="form-group">
            <label>Adresse du partenaire</label>
            <input type="text" class="form-control" name="adresse"  placeholder="">
          </div>

          <div class="form-group">
            <label>Code Postale</label>
            <input type="number" class="form-control" name="cp"  placeholder="">
          </div>

          <div class="form-group">
            <label>Ville</label>
            <input type="text" class="form-control" name="ville"  placeholder="">
          </div>

          <div class="form-group">
            <label>Adresse email</label>
            <input type="email" class="form-control" name="email"  placeholder="">
          </div>

          <button type="submit" class="btn btn-light">Supprimé</button>
    </div>
        <!-- Message de confirmation -->
          <?php
                if(isset($_GET['ajouter'])&& $_GET['ajouter'] == 1){
                  echo "
                  <div class = 'alert alert-danger'>
                  Votre partenaire a été Supprimer
                  </div>";
                 }
          ?>

  </body>
</html>
