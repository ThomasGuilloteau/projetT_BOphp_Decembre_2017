-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 15 déc. 2017 à 15:37
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projett`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `administrateurLogin` varchar(255) DEFAULT NULL,
  `administrateurMdp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`id`, `administrateurLogin`, `administrateurMdp`) VALUES
(1, 'admin', 'admin'),
(2, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `emplacements`
--

DROP TABLE IF EXISTS `emplacements`;
CREATE TABLE IF NOT EXISTS `emplacements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emplacementHorraire` varchar(255) DEFAULT NULL,
  `emplacementDate` date DEFAULT NULL,
  `emplacementAdresse` varchar(255) DEFAULT NULL,
  `emplacementCP` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `equipiers`
--

DROP TABLE IF EXISTS `equipiers`;
CREATE TABLE IF NOT EXISTS `equipiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipierNom` varchar(255) DEFAULT NULL,
  `equipierPrenon` varchar(255) DEFAULT NULL,
  `equipierAge` int(255) DEFAULT NULL,
  `equipierEmail` varchar(255) DEFAULT NULL,
  `equipierPoste` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

DROP TABLE IF EXISTS `evenements`;
CREATE TABLE IF NOT EXISTS `evenements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evenementNom` varchar(255) DEFAULT NULL,
  `evenementDate` date DEFAULT NULL,
  `evenementAdresse` varchar(255) DEFAULT NULL,
  `evenementCP` int(5) DEFAULT NULL,
  `evenementCategorie` varchar(255) DEFAULT NULL,
  `evenementDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `formulaire`
--

DROP TABLE IF EXISTS `formulaire`;
CREATE TABLE IF NOT EXISTS `formulaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formulaireNom` varchar(255) DEFAULT NULL,
  `formulairePrenom` varchar(255) DEFAULT NULL,
  `formulaireEmail` varchar(255) DEFAULT NULL,
  `formulaireTexte` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `formulaire`
--

INSERT INTO `formulaire` (`id`, `formulaireNom`, `formulairePrenom`, `formulaireEmail`, `formulaireTexte`) VALUES
(1, 'khechine', 'ryad', 'khechineryad@hotmail.fr', 'salut'),
(2, 'dd', 'ryad', 'aa@hotmail.fr', 'salut');

-- --------------------------------------------------------

--
-- Structure de la table `formules`
--

DROP TABLE IF EXISTS `formules`;
CREATE TABLE IF NOT EXISTS `formules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formuleNom` varchar(255) DEFAULT NULL,
  `formulePrix` int(255) DEFAULT NULL,
  `formuleDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
CREATE TABLE IF NOT EXISTS `partenaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partenaireNom` varchar(255) DEFAULT NULL,
  `partenaireAdresse` varchar(255) DEFAULT NULL,
  `partenaireVille` varchar(255) DEFAULT NULL,
  `partenaireCP` int(5) DEFAULT NULL,
  `partenaireEmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
